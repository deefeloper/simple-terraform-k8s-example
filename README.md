# simple-terraform-k8s-example


## Getting started

Want to deploy your k8s locally like a pro but need a starting point?
Well  here ya go! just run local-deploy.sh and enjoy your first locally ran kubernetes cluster.
You will need to setup both kind and terraform for this. 


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
MIT License, Joeri Jungschlager

## Project status
A DevOps skeleton for setting up projects/deployments  actively mainted in 2024 compliant with the latest terraform/k8s standards 

!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/jdrk9)